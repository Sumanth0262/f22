class Article < ApplicationRecord
 has_many :comments
 acts_as_votable

 def self.liked article,user
 article.liked_by user
 end

 def self.disliked article,user
  article.downvote_from user
 end

 def self.no_of_likes article
  ActsAsVotable::Vote.where(votable_id: article.id,vote_flag: true).count
 end

 def self.no_of_dislikes article
  ActsAsVotable::Vote.where(votable_id: article.id,vote_flag: false).count
 end

 def self.get_email article
  User.where(id:article.user_id).first.email
 end

end
