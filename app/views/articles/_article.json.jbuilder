json.extract! article, :id, :article_link, :article_text, :article_title, :article_description, :user_id, :is_admin, :created_at, :updated_at
json.url article_url(article, format: :json)
