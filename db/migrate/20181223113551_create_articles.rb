class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :article_link
      t.text :article_text
      t.text :article_title
      t.string :article_description
      t.integer :user_id
      t.boolean :is_admin

      t.timestamps
    end
  end
end
