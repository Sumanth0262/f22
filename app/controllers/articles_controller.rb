class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all
    @no_articles = Article.all.empty?
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)
    respond_to do |format|
      if @article.save
        format.html { redirect_to '/articles'}
        format.json { render :show, status: :created, location: @article }
        flash[:success] = "Thank You for Creating Article.."
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to '/articles'}
        format.json { render :show, status: :ok, location: @article }
        flash[:warning] = "Article Updated Successfully..."
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url}
      format.json { head :no_content }
      flash[:success] = 'Article was successfully destroyed.'
    end
  end

  def create_like
        @article = Article.where(id:params.to_unsafe_h[:format])
        @user = current_user
        Article.liked @article.first,@user
        flash[:warning] = 'Thanks for upvoting'
    redirect_to '/articles'
  end

  def create_dislike
    @article = Article.where(id:params.to_unsafe_h[:format])
    @user = current_user
    Article.disliked @article.last,@user
    flash[:danger] = "Hope we come back with good Article!Thank You for Vote."
    redirect_to '/articles'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:article_link, :article_text, :article_title, :article_description, :user_id, :is_admin)
    end
end
