module SessionsHelper
  def log_in (user_id)
     session[:user_id] = user_id
  end

  def current_user
    if session[:user_id]
      User.find_by(id: session[:user_id])
    end
  end

  def user_signed_in
    Session.where(user_id:session[:user_id]).exists?
  end

  def log_out
    Session.where(user_id:session[:user_id]).destroy_all
    flash[:danger] = "Signed out Successfully..."
  end
end
