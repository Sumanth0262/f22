Rails.application.routes.draw do
  resources :articles
  resources :users
  resources :sessions
  resources :comments
  root 'main#main'
  get "session_create" => 'sessions#create' , via: [:get, :post]
  delete '/logout',  to: 'sessions#destroy' , via:[:get, :post]
  get "comment_form" => 'comments#create', via: [:get, :post]
  get "create_like" => 'articles#create_like'
  get "create_dislike" => 'articles#create_dislike'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
