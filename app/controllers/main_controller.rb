class MainController < ApplicationController
  def main
      if(params[:session].present?)
        user_exist= User.where(email:params[:session][:email],password:params[:session][:password]).last
        if user_exist.present?
          # creating session
          Session.create(user_id: user_exist.id,email:user_exist.email)
          flash[:success] = "Welcome to the F22 POSTS!"
          log_in user_exist.id
          redirect_to '/articles'
        else
          flash[:danger] = "Please Check Your credentials"
          redirect_to root_path
        end
      end
  end
end
